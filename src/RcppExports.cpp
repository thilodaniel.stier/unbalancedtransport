// Generated by using Rcpp::compileAttributes() -> do not edit by hand
// Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#include <RcppArmadillo.h>
#include <Rcpp.h>

using namespace Rcpp;

#ifdef RCPP_USE_GLOBAL_ROSTREAM
Rcpp::Rostream<true>&  Rcpp::Rcout = Rcpp::Rcpp_cout_get();
Rcpp::Rostream<false>& Rcpp::Rcerr = Rcpp::Rcpp_cerr_get();
#endif

// StabilizedScaling_Rcpp
Rcpp::List StabilizedScaling_Rcpp(const arma::mat& cost_matrix, const arma::vec& supply, const arma::vec& demand, int supply_div_type, int demand_div_type, const arma::vec supply_div_parameters, const arma::vec demand_div_parameters, int iter_max, const arma::vec& epsvec, double tol, int check_interval);
RcppExport SEXP _unbalancedTransport_StabilizedScaling_Rcpp(SEXP cost_matrixSEXP, SEXP supplySEXP, SEXP demandSEXP, SEXP supply_div_typeSEXP, SEXP demand_div_typeSEXP, SEXP supply_div_parametersSEXP, SEXP demand_div_parametersSEXP, SEXP iter_maxSEXP, SEXP epsvecSEXP, SEXP tolSEXP, SEXP check_intervalSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< const arma::mat& >::type cost_matrix(cost_matrixSEXP);
    Rcpp::traits::input_parameter< const arma::vec& >::type supply(supplySEXP);
    Rcpp::traits::input_parameter< const arma::vec& >::type demand(demandSEXP);
    Rcpp::traits::input_parameter< int >::type supply_div_type(supply_div_typeSEXP);
    Rcpp::traits::input_parameter< int >::type demand_div_type(demand_div_typeSEXP);
    Rcpp::traits::input_parameter< const arma::vec >::type supply_div_parameters(supply_div_parametersSEXP);
    Rcpp::traits::input_parameter< const arma::vec >::type demand_div_parameters(demand_div_parametersSEXP);
    Rcpp::traits::input_parameter< int >::type iter_max(iter_maxSEXP);
    Rcpp::traits::input_parameter< const arma::vec& >::type epsvec(epsvecSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    Rcpp::traits::input_parameter< int >::type check_interval(check_intervalSEXP);
    rcpp_result_gen = Rcpp::wrap(StabilizedScaling_Rcpp(cost_matrix, supply, demand, supply_div_type, demand_div_type, supply_div_parameters, demand_div_parameters, iter_max, epsvec, tol, check_interval));
    return rcpp_result_gen;
END_RCPP
}
// Sinkhorn_Rcpp
Rcpp::List Sinkhorn_Rcpp(const arma::mat& cost_matrix, const arma::vec& supply, const arma::vec& demand, int supply_div_type, int demand_div_type, const arma::vec supply_div_parameters, const arma::vec demand_div_parameters, int iter_max, const arma::vec& epsvec, double tol, int thread_cnt, int max_lines_per_work);
RcppExport SEXP _unbalancedTransport_Sinkhorn_Rcpp(SEXP cost_matrixSEXP, SEXP supplySEXP, SEXP demandSEXP, SEXP supply_div_typeSEXP, SEXP demand_div_typeSEXP, SEXP supply_div_parametersSEXP, SEXP demand_div_parametersSEXP, SEXP iter_maxSEXP, SEXP epsvecSEXP, SEXP tolSEXP, SEXP thread_cntSEXP, SEXP max_lines_per_workSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< const arma::mat& >::type cost_matrix(cost_matrixSEXP);
    Rcpp::traits::input_parameter< const arma::vec& >::type supply(supplySEXP);
    Rcpp::traits::input_parameter< const arma::vec& >::type demand(demandSEXP);
    Rcpp::traits::input_parameter< int >::type supply_div_type(supply_div_typeSEXP);
    Rcpp::traits::input_parameter< int >::type demand_div_type(demand_div_typeSEXP);
    Rcpp::traits::input_parameter< const arma::vec >::type supply_div_parameters(supply_div_parametersSEXP);
    Rcpp::traits::input_parameter< const arma::vec >::type demand_div_parameters(demand_div_parametersSEXP);
    Rcpp::traits::input_parameter< int >::type iter_max(iter_maxSEXP);
    Rcpp::traits::input_parameter< const arma::vec& >::type epsvec(epsvecSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    Rcpp::traits::input_parameter< int >::type thread_cnt(thread_cntSEXP);
    Rcpp::traits::input_parameter< int >::type max_lines_per_work(max_lines_per_workSEXP);
    rcpp_result_gen = Rcpp::wrap(Sinkhorn_Rcpp(cost_matrix, supply, demand, supply_div_type, demand_div_type, supply_div_parameters, demand_div_parameters, iter_max, epsvec, tol, thread_cnt, max_lines_per_work));
    return rcpp_result_gen;
END_RCPP
}
// treegkr_Rcpp
Rcpp::List treegkr_Rcpp(Rcpp::List& tree, Rcpp::NumericVector& supply, Rcpp::NumericVector& demand, Rcpp::NumericVector& creation, Rcpp::NumericVector& destruction);
RcppExport SEXP _unbalancedTransport_treegkr_Rcpp(SEXP treeSEXP, SEXP supplySEXP, SEXP demandSEXP, SEXP creationSEXP, SEXP destructionSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< Rcpp::List& >::type tree(treeSEXP);
    Rcpp::traits::input_parameter< Rcpp::NumericVector& >::type supply(supplySEXP);
    Rcpp::traits::input_parameter< Rcpp::NumericVector& >::type demand(demandSEXP);
    Rcpp::traits::input_parameter< Rcpp::NumericVector& >::type creation(creationSEXP);
    Rcpp::traits::input_parameter< Rcpp::NumericVector& >::type destruction(destructionSEXP);
    rcpp_result_gen = Rcpp::wrap(treegkr_Rcpp(tree, supply, demand, creation, destruction));
    return rcpp_result_gen;
END_RCPP
}

static const R_CallMethodDef CallEntries[] = {
    {"_unbalancedTransport_StabilizedScaling_Rcpp", (DL_FUNC) &_unbalancedTransport_StabilizedScaling_Rcpp, 11},
    {"_unbalancedTransport_Sinkhorn_Rcpp", (DL_FUNC) &_unbalancedTransport_Sinkhorn_Rcpp, 12},
    {"_unbalancedTransport_treegkr_Rcpp", (DL_FUNC) &_unbalancedTransport_treegkr_Rcpp, 5},
    {NULL, NULL, 0}
};

RcppExport void R_init_unbalancedTransport(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
